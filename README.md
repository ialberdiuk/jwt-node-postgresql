# JWT-Node-PostgreSQL

## Description
Good starting point (boilerplate) to create a sign in using JWT authentication. It relies on PostgreSQL, JWT, Express, and Mocha for Unit Tests <strong> mocking the DB </strong>

## Installation
npm i

npm start

npm test

http://127.0.0.1:5001/ and add paths and payloads as described above

## Usage
<b>api/v1/signup POST</b> (Check if user already exists and username and password are not null and String in 

DB otherwise it will register)

```
{
    username: "string",
    password: "string"
}
```

<b>api/v1/signin POST</b>  (To get the token once DB returns user.Id)

```
{
    "username": "string",
    "email": "string",
    "password": "string"
}




