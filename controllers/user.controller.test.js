const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;
const faker = require('faker');
const UserController = require('./user.controller');
const UserService = require('../services/user.service');

describe.only('UserController', () => {
  describe('register a new user', () => {
    let status; 
    let json; 
    let res;
    beforeEach(() => {
      status = sinon.stub();
      json = sinon.spy();
      res = { json, status };
      status.returns(res);
    });
    it('should not register a user when name param is not provided', async () => {
      const req = { body: { email: faker.internet.email() } };

      await UserController.signUp(req, res);
      expect(status.calledOnce).to.be.true;
      expect(status.args[0][0]).to.equal(400);
      expect(json.calledOnce).to.be.true;
      expect(json.args[0][0].message).to.equal('Invalid Params');
    });

    it('should not register a user when name and email params are not provided', async () => {
      const req = { body: {} };

      await UserController.signUp(req, res);

      expect(status.calledOnce).to.be.true;
      expect(status.args[0][0]).to.equal(400);
      expect(json.calledOnce).to.be.true;
      expect(json.args[0][0].message).to.equal('Invalid Params');
    });

    it('should not register a user when email param is not provided', async () => {
      const req = { body: { name: faker.name.findName() } };

      await UserController.signUp(req, res);

      expect(status.calledOnce).to.be.true;
      expect(status.args[0][0]).to.equal(400);
      expect(json.calledOnce).to.be.true;
      expect(json.args[0][0].message).to.equal('Invalid Params');
    });

    it('should register a user when email, username and password are provided', async () => {
      const req = {
        body: { name: faker.name.findName(), email: faker.internet.email(), password: 'asdshd87324' },
      };

      const stubValue = {
        name: faker.name.findName(),
        email: faker.internet.email(),
        hashed_password: 'asdshd87324'
      };
      const stub = sinon.stub(UserService, "create").returns(stubValue);
      await UserController.register(req, res);
      expect(stub.calledOnce).to.be.true;
      expect(status.calledOnce).to.be.true;
      expect(status.args[0][0]).to.equal(201);
      expect(json.calledOnce).to.be.true;
      expect(json.args[0][0].data).to.equal(stubValue);
    });
  });

  describe('getUser', () => {
    let req;
    let res;
    beforeEach(() => {
      req = { params: { id: faker.random.uuid() } };
      res = { json() {} };
    });

    it('should return a user that matches the id param', async () => {
      const stubValue = {
        id: req.params.id,
        name: faker.name.findName(),
        email: faker.internet.email(),
        createdAt: faker.date.past(),
        updatedAt: faker.date.past(),
      };
      const mock = sinon.mock(res);
      mock
        .expects('json')
        .once()
        .withExactArgs({ data: stubValue });

      const stub = sinon.stub(UserService, "getUser").returns(stubValue);
      await UserController.getUser(req, res);
      expect(stub.calledOnce).to.be.true;
      mock.verify();
    });
  });
});
