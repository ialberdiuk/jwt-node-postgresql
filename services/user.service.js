const UserRepository = require('../db/user.repository');

exports.create = async (name, email, hashed_password) => {
    return UserRepository.create(name, email, hashed_password);
}

exports.getUser = async (id) => {
    return UserRepository.getUser(id);
}

